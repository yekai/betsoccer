pragma solidity^0.4.20;

contract betsoccer {
    address public cto;
    //pan kou 
    struct Game {
        string hostTeam;
        string guestTeam;
        uint16 point;// barsil 1.5 france 
    }
    Game public game;
    bool public canReset = true;
    struct Player {
        address addr;
        uint    amount;
    }
    Player[] public teamsA;
    Player[] public teamsB;
    uint totalTeamA;//sum(teamsA)
    uint totalTeamB;
    //construct 
    function betsoccer(address _cto) public {
        cto = _cto;
        canReset = true;
    }
    
    function setGame(string A, string B, uint16 _point) public {
        assert( msg.sender == cto );// must cto 
        assert( canReset );
        game.hostTeam = A;
        game.guestTeam = B;
        game.point = _point;
        canReset = false;
    }
    
    function bet(bool aORb) payable public {
        assert(msg.value > 0);
        Player memory player = Player(msg.sender, msg.value);
        if( aORb ) {
            // hostTeam 
            teamsA.push(player);
            totalTeamA += msg.value;
            
        }
        else {
            // guestTeam
            teamsB.push(player);
            totalTeamB += msg.value;
        }
    } 
    //kai jiang 
    function openGame(uint8 A, uint8 B) payable public {
        assert( msg.sender == cto );// must cto 
        // 50,150,250 ++++ 1 :0 0.5 
        Player player;
        uint i ;
        if( A *100 - game.point > B*100 ) {
            //host win 
            //fen qian an bi li 
            for(i = 0 ; i < teamsA.length ; i ++ ) {
                player = teamsA[i];
                player.addr.transfer( totalTeamB *player.amount *90 /totalTeamA/100 + player.amount );
            }
            cto.transfer(totalTeamB * 10 / 100);
            
        }
        else {
            //guset win 
            for(i = 0 ; i < teamsB.length ; i ++ ) {
                player = teamsB[i];
                player.addr.transfer( totalTeamA *player.amount *90 /totalTeamB/100 + player.amount );
            }
            cto.transfer(totalTeamA * 10 / 100);
        }
        
        canReset = true;
        
    }
    
    function getBalance() public view returns (uint) {
        return address(this).balance;
    }
    
}
